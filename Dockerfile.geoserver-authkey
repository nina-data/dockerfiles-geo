ARG GEOSERVER_VERSION=2.17.0
ARG GEOSERVER_HOME=/usr/share/geoserver

FROM alpine:3.11 AS builder

ARG GEOSERVER_VERSION
ARG GEOSERVER_HOME

ENV GEOSERVER_LIBS $GEOSERVER_HOME/webapps/geoserver/WEB-INF/lib
RUN \
    mkdir -p $GEOSERVER_HOME && \
    wget -q https://downloads.sourceforge.net/project/geoserver/GeoServer/$GEOSERVER_VERSION/geoserver-$GEOSERVER_VERSION-bin.zip && \
    unzip -qd $GEOSERVER_HOME geoserver-$GEOSERVER_VERSION-bin.zip && \
    rm geoserver-$GEOSERVER_VERSION-bin.zip && \
    for dir in coverages data layergroups palettes workspaces; do \
        rm -r $GEOSERVER_HOME/data_dir/$dir/*; \
    done && \
    wget -q https://sourceforge.net/projects/geoserver/files/GeoServer/$GEOSERVER_VERSION/extensions/geoserver-$GEOSERVER_VERSION-authkey-plugin.zip && \
    unzip -qod $GEOSERVER_LIBS geoserver-$GEOSERVER_VERSION-authkey-plugin.zip && \
    rm geoserver-$GEOSERVER_VERSION-authkey-plugin.zip && \
    :

# libfreetype6 is missing from 8-jre-slim-stretch
# https://github.com/docker-library/openjdk/issues/333
FROM openjdk:8-jre-stretch

ARG GEOSERVER_VERSION
ARG GEOSERVER_HOME

#https://github.com/moby/moby/issues/35018
COPY --from=builder --chown=1001:1001 $GEOSERVER_HOME $GEOSERVER_HOME
WORKDIR $GEOSERVER_HOME
RUN \
    groupadd --system --gid 1001 geoserver && \
    useradd --system --no-create-home --uid 1001 --gid geoserver geoserver && \
    ln -s $PWD/bin/startup.sh /entrypoint.sh && \
    chmod og+x bin/startup.sh /entrypoint.sh && \
    :

EXPOSE 8080/tcp
#VOLUME $GEOSERVER_HOME/data_dir
USER geoserver
CMD ["/entrypoint.sh"]
