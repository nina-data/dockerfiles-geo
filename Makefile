DOCKER ?= podman
OPTS ?= --no-cache
DOCKERFILES ?= $(wildcard Dockerfile.*)
TARGETS = $(DOCKERFILES:Dockerfile.%=%)
.PHONY: all Dockerfile.%

all: $(TARGETS)

geoserver-sqlserver: geoserver
postgis-fdw-tds:

$(TARGETS):
	$(DOCKER) build -f Dockerfile.$@ -t $@ $(OPTS) .

geoserver:
	$(DOCKER) build -f Dockerfile.$@ -t $@ -t $@:2.15.2 $(OPTS) .
